<?php include'layout/header.php' ?>

	<!-- Banner Slide Show -->
	<div id="carousel-example-generic" style="margin-top: -25px;" class="carousel slide hidden-sm hidden-xs" data-ride="carousel">
        <!-- Indicators -->
	    <ol class="carousel-indicators">
		    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
		    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
	    </ol>

	    <!-- Wrapper for slides -->
	    <div class="carousel-inner" role="listbox">

	    	<!-- Strat repeat banner 1 (for lg and md) -->
	      	<div class="item active">
	        	<img src="../dist/img/banner.jpg">
	        	<div class="carousel-caption">
		          	<div class="caption-slider">
			            <div class="caption-date">5 Mei 2015</div>
			            <div class="caption-description">Kunjungan Menteri<br> Pemberdayaan Aparatur<br>
			            Negara, Yuddy Chrisnandi</div>
		          	</div>
		        </div>
		    </div>
		    <!-- End repeat -->

		    <div class="item">
		       	<img src="../dist/img/banner.jpg">
	        	<div class="carousel-caption">
	          		<div class="caption-slider">
		            	<div class="caption-date">5 Mei 2015</div>
		            	<div class="caption-description">Kunjungan Menteri<br> Pemberdayaan Aparatur<br>
		            	Negara, Yuddy Chrisnandi</div>
		          	</div>
		        </div>
		    </div>
		    <div class="item">
		       	<img src="../dist/img/banner.jpg">
	        	<div class="carousel-caption">
	          		<div class="caption-slider">
		            	<div class="caption-date">5 Mei 2015</div>
		            	<div class="caption-description">Kunjungan Menteri<br> Pemberdayaan Aparatur<br>
		            	Negara, Yuddy Chrisnandi</div>
		          	</div>
		        </div>
		    </div>
		</div>

		<!-- Controls -->
		<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>

	<!-- Slider di frame -xs -->
	<div id="carousel-example-generic" style="margin-top: -25px;" class="carousel slide hidden-lg hidden-md" data-ride="carousel">
        <!-- Indicators -->
	    <ol class="carousel-indicators">
		    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
		    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
	    </ol>

	    <!-- Wrapper for slides -->
	    <div class="carousel-inner" role="listbox">

	    	<!-- Strat repeat banner 1 (for sm and xs) -->
	      	<div class="item active">
	        	<img src="../dist/img/banner.jpg">
	        	<div class="carousel-caption">
		          	<div class="caption-slider-sm">
			            <div class="caption-date">5 Mei 2015</div>
			            <div class="caption-description">Kunjungan Menteri<br> Pemberdayaan Aparatur<br>
			            Negara, Yuddy Chrisnandi</div>
		          	</div>
		        </div>
		    </div>
		    <!-- End repeat -->

		    <div class="item">
		       	<img src="../dist/img/banner.jpg">
	        	<div class="carousel-caption">
	          		<div class="caption-slider-sm">
		            	<div class="caption-date">5 Mei 2015</div>
		            	<div class="caption-description">Kunjungan Menteri<br> Pemberdayaan Aparatur<br>
		            	Negara, Yuddy Chrisnandi</div>
		          	</div>
		        </div>
		    </div>
		    <div class="item">
		       	<img src="../dist/img/banner.jpg">
	        	<div class="carousel-caption">
	          		<div class="caption-slider">
		            	<div class="caption-date">5 Mei 2015</div>
		            	<div class="caption-description">Kunjungan Menteri<br> Pemberdayaan Aparatur<br>
		            	Negara, Yuddy Chrisnandi</div>
		          	</div>
		        </div>
		    </div>
		</div>

		<!-- Controls -->
		<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
	<!-- End Banner Slide Show -->

	<!-- For lg md sm size -->
	<div class="content-menu hidden-xs">
	    <div class="container" style="margin-top: 10px;">
	      	<div class="col-sm-3 content-menu-m" style="border-right: 2px solid #ddd;">
	        	<div class="form-group">
	          		<div class="col-md-5 col-sm-12 col-xs-12 content-image">
	            		<img class="img-responsive" src="../dist/img/image-item-1.jpg">
	          		</div>
	          		<div class="col-md-7 col-sm-12 col-xs-12 content-title">
	            		Aplikasi<br>Susulan Paket
	          		</div>
	        	</div>
	      	</div>
	      	<div class="col-sm-3 content-menu-m">
	        	<div class="form-group">
	          		<div class="col-md-5 col-sm-12 col-xs-12 content-image">
	            		<img class="img-responsive" src="../dist/img/image-item-2.jpg">
	          		</div>
	         	 	<div class="col-md-7 col-sm-12 col-xs-12 content-title">
	            		Aplikasi<br>Pengembangan<br>Paket
	          		</div>
	        	</div>
	      	</div>
	      	<div class="col-sm-3 content-menu-m" style="border-left: 2px solid #ddd; border-right: 2px solid #ddd;">
	        	<div class="form-group">
	          		<div class="col-md-5 col-sm-12 col-xs-12 content-image">
	            		<img class="img-responsive" src="../dist/img/image-item-3.jpg">
	          		</div>
	          		<div class="col-md-7 col-sm-12 col-xs-12 content-title">
	            		LPSE<br>Prov. Jatim
	          		</div>
	        	</div>
	      	</div>
	      	<div class="col-sm-3 content-menu-m">
	        	<div class="form-group">
	          		<div class="col-md-5 col-sm-12 col-xs-12 content-image">
	            		<img class="img-responsive" src="../dist/img/image-item-4.jpg">
	          		</div>
	          		<div class="col-md-7 col-sm-12 col-xs-12 content-title">
	            		Apel Baja<br>2016
	          		</div>
	        	</div>
	      	</div>
	    </div>
	</div>
	<!-- End -->

	<!-- For xs size -->
	<div class="content-menu hidden-lg hidden-md hidden-sm">
	    <div class="container" style="margin-top: 10px;">
	    	<div class="col-xs-12 content-menu-m" style="border-bottom: 1px solid #eee">
	    		<div class="form-group">
	    			<div class="col-xs-6 content-image">
	    				<img class="img-responsive" src="../dist/img/image-item-1.jpg">
	    			</div>
	    			<div class="col-xs-6 content-title">
	    				Aplikasi<br>Susulan Paket
	    			</div>
	    		</div>
	    	</div>
	    	<div class="col-xs-12 content-menu-m" style="border-bottom: 1px solid #eee">
	    		<div class="form-group">
	    			<div class="col-xs-6 content-image">
	    				<img class="img-responsive" src="../dist/img/image-item-2.jpg">
	    			</div>
	    			<div class="col-xs-6 content-title">
	    				Aplikasi<br>Pengembangan<br>Paket
	    			</div>
	    		</div>
	    	</div>
	    	<div class="col-xs-12 content-menu-m" style="border-bottom: 1px solid #eee">
	    		<div class="form-group">
	    			<div class="col-xs-6 content-image">
	    				<img class="img-responsive" src="../dist/img/image-item-3.jpg">
	    			</div>
	    			<div class="col-xs-6 content-title">
	    				LPSE<br>Prov. Jatim
	    			</div>
	    		</div>
	    	</div>
	    	<div class="col-xs-12 content-menu-m">
	    		<div class="form-group">
	    			<div class="col-xs-6 content-image">
	    				<img class="img-responsive" src="../dist/img/image-item-4.jpg">
	    			</div>
	    			<div class="col-xs-6 content-title">
	    				Apel Baja<br>2016
	    			</div>
	    		</div>
	    	</div>
	    </div>
	</div>
	<!-- End -->

	<div class="content-body">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-12 col-xs-12 side-car">
					<div class="bunder">
						<div class="label-chart">
							<h3>Progres</h3>
							<h1>Pengadaan</h1>
						</div>
						<div class="donut-chart">
							<canvas id="pieChart" height="250"></canvas>
						</div>
					</div>
					<div class="bar">
						<div class="segitiga"></div>
						<div class="label-chart-bar">
							<h3>Pengadaan</h3>
							<h1>Barang</h1>
						</div>
						<div class="bar-chart">
							<div id="bar-chart" style="height: 300px;"></div>
						</div>
					</div>
				</div>
				<div class="col-md-8 col-sm-12 col-xs-12">
					<div class="row">
						<div class="col-md-12 hot-news">
							<div class="form-group">
								<div class="title-text-pengumuman">
									<a href="page/pengumuman/pengumuman.php">PENGUMUMAN</a>
								</div>
								<div class="col-md-6 hot-news-left">
									<ul>
										<li>
											<a href="#">
												<p>Hasil Rapat Evaluasi UPT P2BJ</p>
												<p style="font-size: 0.7em;">4 Februari 2016</p>
											</a>
										</li>
									</ul>
								</div>
								<div class="col-md-6 hot-news-right">
									<ul>
										<!-- Repeat Menu pengumuman di kanan, menu no/ke 2 memakai class active, max 3 menu -->
										<li><a href="#">Materi 1 Evaluasi UPT P2BJ</a></li>
										<!-- End Repeat -->

										<li><a href="#" class="active">Materi 2 Sesi Kenijakan dan Peraturan P2BJ</a></li>
										<li><a href="#">Materi 3 Standar Operasional Prosedur UPT P2BJ</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<!-- For lg size -->
						<div class="col-md-12 news hidden-md hidden-sm hidden-xs">
							<div class="form-group">
								<div class="title-text"><a href="page/berita/berita.php">BERITA</a></div>
								<div class="col-md-7 news-top-img">
									<img src="../dist/img/news.jpg" class="img-responsive" style="margin-left: -30px;">
								</div>
								<div class="col-md-5 news-top-text">
									<div class="news-top-text-date">10 Mei 2016</div>
									<div class="news-top-text-data">
										Kunjungan Kerja dari Pemprov. Sulawesi Selatan
										<a style="width:100px;" class="button -white" type="button" href="#">BACA</a>
									</div>
								</div>
							</div>
						</div>
						<!-- End -->
						<!-- For md size -->
						<div class="col-md-12 news hidden-lg hidden-sm hidden-xs">
							<div class="form-group">
								<div class="title-text"><a href="page/berita/berita.php">BERITA</a></div>
								<div class="col-md-7 news-top-img-md">
									<img src="../dist/img/news.jpg" class="img-responsive" style="margin-left: -30px;">
								</div>
								<div class="col-md-5 news-top-text">
									<div class="news-top-text-date-md">10 Mei 2016</div>
									<div class="news-top-text-data-md">
										Kunjungan Kerja dari Pemprov. Sulawesi Selatan
										<a style="width:100px;" class="button-md -white" type="button" href="#">BACA</a>
									</div>
								</div>
							</div>
						</div>
						<!-- End -->
						<!-- For sm xs size -->
						<div class="col-md-12 news hidden-lg hidden-md">
							<div class="form-group">
								<div class="title-text"><a href="page/berita/berita.php">BERITA</a></div>
								<div class="col-sm-7 col-xs-6 news-top-img-sm">
									<img src="../dist/img/news.jpg" class="img-responsive" style="margin-left: -30px;">
								</div>
								<div class="col-md-5 col-md-6 news-top-text">
									<div class="news-top-text-date-md">10 Mei 2016</div>
									<div class="news-top-text-data-md">
										Kunjungan Kerja dari Pemprov. Sulawesi Selatan
										<a style="width:100px;" class="button-md -white" type="button" href="#">BACA</a>
									</div>
								</div>
							</div>
						</div>
						<!-- End -->
						<div class="col-md-12 news-bottom">
							<div class="news-bottom-title">
								<span class="grey">BERITA LAINNYA</span>
								<span class="grey pull-right"><a href="#">ARSIP BERITA <i class="fa fa-arrow-right"></i></a></span>
							</div>
							<div class="form-group">
								<div class="news-bottom-content">

									<!-- Repeat berita lainnya, max caracter 53, max 3 content, untuk titik2 diberi 4/3 -->
									<div class="col-md-4">
										<div class="news-bottom-box">
											<img src="../dist/img/news-bottom-box-img-1.jpg" class="img-responsive">
											<div class="news-bottom-box-date">8 Mei 2016</div>
											<div class="news-bottom-box-data">
												<a href="#">Buka Puasa Bersama Seluruh Karyawan UPT-P2BJ TH.2014.... <br /><i style="font-size: 0.8em;">(lanjut membaca)</i></a>
											</div>
										</div>
									</div>
									<!-- End repeat -->

									<div class="col-md-4">
										<div class="news-bottom-box">
											<img src="../dist/img/news-bottom-box-img-2.jpg" class="img-responsive">
											<div class="news-bottom-box-date">8 Mei 2016</div>
											<div class="news-bottom-box-data">
												<a href="#">Buka Puasa Bersama Seluruh Karyawan UPT-P2BJ TH.2014.... <br /><i style="font-size: 0.8em;">(lanjut membaca)</i></a>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="news-bottom-box">
											<img src="../dist/img/news-bottom-box-img-3.jpg" class="img-responsive">
											<div class="news-bottom-box-date">8 Mei 2016</div>
											<div class="news-bottom-box-data">
												<a href="#">Buka Puasa Bersama Seluruh Karyawan UPT-P2BJ TH.2014.... <br /><i style="font-size: 0.8em;">(lanjut membaca)</i></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div><!-- End Row -->

			<div class="row">
				<!-- For lg md sm size -->
				<div class="col-md-8 col-sm-12 col-xs-12 complaint hidden-xs">
					<div class="title-text-complaint margin-min"><a href="page/pengaduan/pengaduan.php">PENGADUAN</a></div>
					<div class="news-complaint-box">
						<div class="form-group">

							<!-- Repeat pengaduan max caracter 136, max content 3, untuk titik2 3 -->
							<div class="col-md-4 col-sm-4 col-xs-12">
								<div class="news-complaint">
									<div class="news-bottom-box-date">Dian, Malang</div>
									<div class="news-complaint-text">
										This is the most basic form of Bootstrap: 
									</div>
									<div class="news-complaint-answer">Jawaban :</div>
									<div class="news-complaint-text-answer">
										This is the most basic form of Bootstrap: precompiled files precompiled files
										<a href="pengaduan">BACA</a>
									</div>
								</div>
							</div>
							<!-- End Repeat -->

							<div class="col-md-4 col-sm-4 col-xs-12">
								<div class="news-complaint">
									<div class="news-bottom-box-date">Dian, Malang</div>
									<div class="news-complaint-text">
										This is the most basic form of Bootstrap: precompiled files for quick drop-in usage in nearly any web project.
									</div>
									<div class="news-complaint-answer">Jawaban :</div>
									<div class="news-complaint-text-answer">
										This is the most basic form of Bootstrap: precompiled files precompiled files
										<a href="pengaduan">BACA</a>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<div class="news-complaint">
									<div class="news-bottom-box-date">Dian, Malang</div>
									<div class="news-complaint-text">
										This is the most basic form of Bootstrap: precompiled files for quick drop-in usage in nearly any web project for quick drop-in usage in...
									</div>
									<div class="news-complaint-answer">Jawaban :</div>
									<div class="news-complaint-text-answer">
										This is the most basic form of Bootstrap: precompiled files precompiled files
										<a href="pengaduan">BACA</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- End -->

				<!-- For xs size -->
				<div class="col-md-8 col-sm-12 col-xs-12 complaint-xs hidden-lg hidden-md hidden-sm">
					<div class="title-text-complaint margin-min"><a href="page/pengaduan/pengaduan.php">PENGADUAN</a></div>
					<div class="news-complaint-box">
						<div class="form-group">

							<!-- Repeat pengaduan max caracter 136, max content 3, untuk titik2 3 -->
							<div class="col-md-4 col-sm-4 col-xs-12">
								<div class="news-complaint">
									<div class="news-bottom-box-date">Dian, Malang</div>
									<div class="news-complaint-text">
										This is the most basic form of Bootstrap: 
									</div>
									<div class="news-complaint-answer">Jawaban :</div>
									<div class="news-complaint-text-answer">
										This is the most basic form of Bootstrap: precompiled files precompiled files
										<a href="pengaduan">BACA</a>
									</div>
								</div>
							</div>
							<!-- End repeat -->

							<div class="col-md-4 col-sm-4 col-xs-12">
								<div class="news-complaint">
									<div class="news-bottom-box-date">Dian, Malang</div>
									<div class="news-complaint-text">
										This is the most basic form of Bootstrap: precompiled files for quick drop-in usage in nearly any web project.
									</div>
									<div class="news-complaint-answer">Jawaban :</div>
									<div class="news-complaint-text-answer">
										This is the most basic form of Bootstrap: precompiled files precompiled files
										<a href="pengaduan">BACA</a>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<div class="news-complaint">
									<div class="news-bottom-box-date">Dian, Malang</div>
									<div class="news-complaint-text">
										This is the most basic form of Bootstrap: precompiled files for quick drop-in usage in nearly any web project for quick drop-in usage in...
									</div>
									<div class="news-complaint-answer">Jawaban :</div>
									<div class="news-complaint-text-answer">
										This is the most basic form of Bootstrap: precompiled files precompiled files
										<a href="pengaduan">BACA</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-12 col-xs-12 form-complaint">
					<div class="">
						<div class="form-group">
							<div class="form-complaint-title">
								Punya keluhan soal proses lelang ? Laporkan pada kami disini!
							</div>
							<div class="form-complaint-input">
								<form class="form" role="form" method="post" enctype="multipart/form-data" id="pengaduan">
									<p class="email">
										<input name="email" type="text" class="validate[required,custom[email]] feedback-input" id="email" placeholder="email" />
									</p>

									<p class="text">
										<textarea name="pengaduan" class="validate[required,length[6,300]] feedback-input" id="comment" placeholder="sampaikan keluhan disini..."></textarea>
									</p>

									<button type="button" class="button -red pull-right">KIRIM</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="gallery-info">
						<div class="title-text-galeri"><a href="page/galeri/galeri.php">GALERI</a></div>
						<div class="gallery-info-photo">
							<ul class="pgwSlideshow">

								<!-- Repeat Galeri -->
								<li>
									<img src="../dist/img/image-gallery.jpg" alt="14 APRIL 2016" data-description="Rapat Evaluasi Pengadaan Barang/Jasa">
								</li>
								<!-- End repeat -->

								<li>
									<img src="../dist/img/banner.jpg" alt="14 APRIL 2016" data-description="Pengesahan Standarisasi Prosedur Operasional & Pelayanan (ISO)">
								</li>
								<li>
									<img src="../dist/img/image-gallery-1.jpg" alt="14 APRIL 2016" data-description="Rapat Evaluasi Pengadaan Barang/Jasa">
								</li>
								<li>
									<img src="../dist/img/image-gallery-2.jpg" alt="14 APRIL 2016" data-description="Pengesahan Standarisasi Prosedur Operasional & Pelayanan (ISO)">
								</li>
								<li>
									<img src="../dist/img/image-gallery-3.jpg" alt="14 APRIL 2016" data-description="Rapat Evaluasi Pengadaan Barang/Jasa">
								</li>
								<li>
									<img src="../dist/img/banner.jpg" alt="14 APRIL 2016" data-description="Pengesahan Standarisasi Prosedur Operasional & Pelayanan (ISO)">
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<footer class="footer-i hidden-sm hidden-xs">
		<div class="container">
			<div class="col-md-12">
				&copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
			</div>
		</div>
	</footer>

	<footer class="footer-xs-i hidden-lg hidden-md">
		<div class="container">
			<div class="col-md-12">
				&copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
			</div>
		</div>
	</footer>

<?php include'layout/footer.php' ?>