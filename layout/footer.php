  </div>
  <script type="text/javascript" src="../../dist/js/jquery-1.11.2.min.js"></script>
  <script type="text/javascript" src="../../dist/plugins/bootstrap/js/bootstrap.min.js"></script>
  <!-- CHARTJS CHARTS -->
  <script type="text/javascript" src="../../dist/plugins/chartjs/Chart.min.js"></script>
  <!-- FLOT CHARTS -->
  <script type="text/javascript" src="../../dist/plugins/flot/jquery.flot.min.js"></script>
  <!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
  <script type="text/javascript" src="../../dist/plugins/flot/jquery.flot.resize.min.js"></script>
  <!-- FLOT PIE PLUGIN - also used to draw donut charts -->
  <script type="text/javascript" src="../../dist/plugins/flot/jquery.flot.pie.min.js"></script>
  <!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
  <script type="text/javascript" src="../../dist/plugins/flot/jquery.flot.categories.min.js"></script>
  <script type="text/javascript" src="../../dist/plugins/easydropdown-master/jquery.easydropdown.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="../../dist/plugins/nicescoll/jquery.nicescroll.min.js" type="text/javascript"></script>
  <!-- Gallery Img -->
  <script type="text/javascript" src="../../dist/plugins/pgw-slideshow-master/pgwslideshow.min.js"></script>

  <script>
    $(function () {
      //-------------
      //- PIE CHART -
      //-------------
      // Get context with jQuery - using jQuery's .get() method.
      var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
      var pieChart = new Chart(pieChartCanvas);
      var PieData = [
      {
        value: 700,
        color: "#f56954",
        highlight: "#f56954",
        label: "Chrome"
      },
      {
        value: 500,
        color: "#00a65a",
        highlight: "#00a65a",
        label: "IE"
      },
      {
        value: 400,
        color: "#f39c12",
        highlight: "#f39c12",
        label: "FireFox"
      },
      {
        value: 600,
        color: "#00c0ef",
        highlight: "#00c0ef",
        label: "Safari"
      },
      {
        value: 300,
        color: "#3c8dbc",
        highlight: "#3c8dbc",
        label: "Opera"
      },
      {
        value: 100,
        color: "#d2d6de",
        highlight: "#d2d6de",
        label: "Navigator"
      }
      ];
      var pieOptions = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke: true,
        //String - The colour of each segment stroke
        segmentStrokeColor: "#fff",
        //Number - The width of each segment stroke
        segmentStrokeWidth: 2,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps: 100,
        //String - Animation easing effect
        animationEasing: "easeOutBounce",
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate: true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale: false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive: true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: false,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
        };
      //Create pie or douhnut chart
      // You can switch between pie and douhnut using the method below.
      pieChart.Doughnut(PieData, pieOptions);
    });
  </script>

  <!-- Page script -->
  <script type="text/javascript">

    $(function () {
    /*
     * BAR CHART
     * ---------
     */

      var bar_data = {
        data: [["Pengajuan <h1>(24)</h1>", 24], ["Bermasalah <h1>(3)</h1>", 3], ["Terverifikasi <h1>(15)</h1>", 15], ["Selesai <h1>(19)</h1>", 19]],
        color: "#18BCBE"
      };
      $.plot("#bar-chart", [bar_data], {
        grid: {
          borderWidth: 1,
          borderColor: "#f3f3f3",
          tickColor: "#f3f3f3"
        },
        series: {
          bars: {
            show: true,
            barWidth: 0.8,
            align: "center"
          }
        },
        xaxis: {
          mode: "categories",
          tickLength: 0
        }
      });
    /* END BAR CHART */
    });

    /*
     * Custom Label formatter
     * ----------------------
     */
    function labelFormatter(label, series) {
      return "<div style='font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;'>"
      + label
      + "<br/>"
      + Math.round(series.percent) + "%</div>";
    }

    $(document).ready(function() {
      $('.pgwSlideshow').pgwSlideshow();
    });
  </script>
</body>
</html>