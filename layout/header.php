<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>P2BJ</title>
	<link rel="stylesheet" type="text/css" href="../../dist/plugins/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="../../dist/plugins/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="../../dist/plugins/pgw-slideshow-master/pgwslideshow.min.css">
	<link rel="stylesheet" type="text/css" href="../../dist/plugins/ionicons-2.0.1/css/ionicons.min.css" />
	<link rel="stylesheet" type="text/css" href="../../dist/plugins/easydropdown-master/themes/easydropdown.css"/>
	<link rel="stylesheet" type="text/css" href="../../dist/plugins/easydropdown-master/themes/easydropdown.metro.css"/>
	<link rel="stylesheet" type="text/css" href="../../dist/css/style.css">
</head>
<body>
<div class="container-j">
	<nav class="navbar navbar-default hidden-xs hidden-sm" style="background: url('../../dist/img/nav-bg.jpg') no-repeat left; background-color: #EB2228;">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<span style="background: url('../../dist/img/nav-bg.jpg') no-repeat center; height: 50px;"></span>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#">HOME</a></li>
					<li><a href="../page/paket_siap_lelang/paket_siap_lelang.php">PAKET SIAP LELANG</a></li>
					<li><a href="../page/proses_lelang/proses_lelang.php">PROSES LELANG</a></li>
					<li><a href="#">PROFILE</a></li>
					<li><a href="../page/galeri/galeri.php">GALERI</a></li>
					<li>
                        <form action="">
        				<div class="search">
        				        <input name="key" id="input-search" type="text" value="" class="input-search" placeholder="Search"/>
        				</div>
                        </form>
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>

	<nav class="navbar navbar-default hidden-lg hidden-md hidden-xs" style="background: url('../../dist/img/nav-bg.jpg') no-repeat left; background-color: #EB2228;">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<span style="background: url('../../dist/img/nav-bg.jpg') no-repeat center; height: 50px;"></span>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#">HOME</a></li>
					<li><a href="#">PAKET SIAP <br /> LELANG</a></li>
					<li><a href="#">PROSES <br /> LELANG</a></li>
					<li><a href="#">PROFILE</a></li>
					<li><a href="#">GALERI</a></li>
					<li>
				        <a href="#" class="dropdown-toggle" data-toggle="dropdown">SEARCH <span class="caret"></span></a>
				        <ul class="dropdown-menu" role="menu">
							<li>
	                            <form action="#">
	                				<div class="search">
	                				    <input name="key" id="input-search-sm" type="text" value="#" class="input-search" placeholder="Search"/>                    
	                				</div>
	                            </form>
							</li>
				        </ul>
			        </li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>


	<nav class="navbar navbar-default hidden-lg hidden-md hidden-sm">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="index.php" class="navbar-brand" style="color: #fff;">P2BJ<font style="font-size: 0.4em;">.com</font></a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#">HOME</a></li>
					<li><a href="#">PAKET SIAP LELANG</a></li>
					<li><a href="#">PROSES LELANG</a></li>
					<li><a href="#">PROFILE</a></li>
					<li><a href="#">GALERI</a></li>
					<li>
                        <form action="#">
        				<div class="search">                
        				        <input name="key" id="input-search-xs" type="text" value="#" class="input-search" placeholder="Search"/>
        				</div>
                        </form>
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>