<?php include'../../layout/header.php' ?>   	

    <div class="content-body-c">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-1 col-sm-2 col-xs-12 left-menu-galeri">
					<ul>
						<li>
							<select id="filterTahunBerita">
                                <!-- Repeat tahun -->
        						<option>2016</option>
                                <!-- End repeat -->
        					</select>
						</li>
                        <!-- Repeat bulan -->
						<li><a class="active" href="#januari">JAN</a></li>
                        <li><a href="#februari">FEB</a></li>
                        <li><a href="#maret">MAR</a></li>
                        <li><a href="#januari">APR</a></li>
                        <li><a href="#mei">MEI</a></li>
                        <li><a href="#juni">JUN</a></li>
                        <li><a href="#juli">JUL</a></li>
                        <li><a href="#agustus">AGS</a></li>
                        <li><a href="#september">SEP</a></li>
                        <li><a href="#oktober">OKT</a></li>
                        <li><a href="#november">NOV</a></li>
                        <li><a href="#desember">DES</a></li>
                        <!-- End repeat -->
    				</ul>
				</div>
				<div class="col-md-4 col-sm-10 col-xs-12 center-menu" id="boxscroll" style="padding: 5px; overflow: auto; height: 550px;">
					<ul>
						<li>
        					<p class="title-center-galeri">Berita</p>
        				</li>

                        <!-- Repeat title menu berita -->
    					<li>
    						<a href="#">
    							<div class="img-tumbnail-berita">
    								<img style="height: 68px;" src="../../dist/img/image-gallery.jpg">
    							</div>
    							<span class="tgl-title">14 Januari 2016</span>
    							<p class="title-center">Rapat Evaluasi Pengadaan Barang/Jasa</p>
                                <br />
    						</a>
    					</li>
                        <!-- End repeat -->

                        <li>
                            <a href="#">
                                <div class="img-tumbnail-berita">
                                    <img style="height: 68px;" src="../../dist/img/image-gallery.jpg">
                                </div>
                                <span class="tgl-title">14 Januari 2016</span>
                                <p class="title-center">Rapat Evaluasi Pengadaan Barang/Jasa</p>
                                <br />
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img-tumbnail-berita">
                                    <img style="height: 68px;" src="../../dist/img/image-gallery.jpg">
                                </div>
                                <span class="tgl-title">14 Januari 2016</span>
                                <p class="title-center">Rapat Evaluasi Pengadaan Barang/Jasa</p>
                                <br />
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img-tumbnail-berita">
                                    <img style="height: 68px;" src="../../dist/img/image-gallery.jpg">
                                </div>
                                <span class="tgl-title">14 Januari 2016</span>
                                <p class="title-center">Rapat Evaluasi Pengadaan Barang/Jasa</p>
                                <br />
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="img-tumbnail-berita">
                                    <img style="height: 68px;" src="../../dist/img/image-gallery.jpg">
                                </div>
                                <span class="tgl-title">14 Januari 2016</span>
                                <p class="title-center">Rapat Evaluasi Pengadaan Barang/Jasa</p>
                                <br />
                            </a>
                        </li>
					</ul>
				</div>
				<div class="col-md-7 col-sm-12 col-xs-12 right-content" id="boxscroll1" style="padding: 5px; overflow: auto; height: 550px;">
                    <!-- Repeat content berita -->
    				<div class="image-galeri-berita">
    					<center><img style="height: 270px;" src="../../dist/img/image-gallery.jpg"></center>
    				</div>
    				<div class="title-galeri-berita">
    					<p class="tgl-title-berita">14 Januari 2016</p>
    					<label class="title-h-berita">Rapat Evaluasi Pengadaan Barang/Jasa</label>
    					<div class="content-galeri-berita">
                            <p class="content-galeri-berita">
                                ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>
                            <p class="content-galeri-berita">
                                ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>
    					</div>
    				</div>
                    <!-- End repeat -->
        		</div>
			</div>
		</div>
	</div>

<footer class="footer hidden-sm hidden-xs">
    <div class="container">
        <div class="col-md-12">
            &copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
        </div>
    </div>
</footer>

<footer class="footer-xs hidden-lg hidden-md">
    <div class="container">
        <div class="col-md-12">
            &copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
        </div>
    </div>
</footer>

<?php include'../../layout/galeri_footer.php' ?>