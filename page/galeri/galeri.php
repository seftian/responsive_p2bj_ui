<?php include'../../layout/header.php' ?>	

    <div class="content-body-c">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-1 col-sm-2 col-xs-12 left-menu-galeri">
					<ul>
    					<li>
                            <select>
                                <!-- Repeat tahun -->
                                <option>2016</option>
                                <!-- End repeat -->
                            </select>
                        </li>
                        <!-- Repeat Bulan -->
                        <li><a class="active" href="#januari">JAN</a></li>
                        <li><a href="#februari">FEB</a></li>
                        <li><a href="#maret">MAR</a></li>
                        <li><a href="#januari">APR</a></li>
                        <li><a href="#mei">MEI</a></li>
                        <li><a href="#juni">JUN</a></li>
                        <li><a href="#juli">JUL</a></li>
                        <li><a href="#agustus">AGS</a></li>
                        <li><a href="#september">SEP</a></li>
                        <li><a href="#oktober">OKT</a></li>
                        <li><a href="#november">NOV</a></li>
                        <li><a href="#desember">DES</a></li>
                        <!-- End repeat -->
    				</ul>
				</div>
				<div class="col-md-3 col-sm-10 col-xs-12 center-menu" id="boxscroll" style="padding: 5px; overflow: auto; height: 550px;">
    				<ul>
						<li>
    						<p class="title-center-galeri">Galeri</p>
    					</li>

                        <!-- Repeat title menu galeri -->
    					<li>
    						<a href="#">
    							<span class="tgl-title">14 Januari 2016</span>
    							<p class="title-center">Rapat Evaluasi Pengadaan Barang/Jasa</p>
    						</a>
    					</li>
                        <!-- End repeat -->

                        <li>
                            <a href="#">
                                <span class="tgl-title">14 Januari 2016</span>
                                <p class="title-center">Rapat Evaluasi Pengadaan Barang/Jasa</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="tgl-title">14 Januari 2016</span>
                                <p class="title-center">Rapat Evaluasi Pengadaan Barang/Jasa</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="tgl-title">14 Januari 2016</span>
                                <p class="title-center">Rapat Evaluasi Pengadaan Barang/Jasa</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="tgl-title">14 Januari 2016</span>
                                <p class="title-center">Rapat Evaluasi Pengadaan Barang/Jasa</p>
                            </a>
                        </li>
    				</ul>
    			</div>
    			<div class="col-md-8 col-sm-12 col-xs-12 right-content">
    				<ul class="pgwSlideshow">

                        <!-- Repeat content galeri -->
    				    <li>
                            <img src="../../dist/img/image-gallery.jpg" alt="14 APRIL 2016" data-description="Rapat Evaluasi Pengadaan Barang/Jasa">
                        </li>
                        <!-- End repeat -->
                        
                        <li>
                            <img src="../../dist/img/banner.jpg" alt="14 APRIL 2016" data-description="Pengesahan Standarisasi Prosedur Operasional & Pelayanan (ISO)">
                        </li>
                        <li>
                        <img src="../../dist/img/image-gallery-1.jpg" alt="14 APRIL 2016" data-description="Rapat Evaluasi Pengadaan Barang/Jasa">
                        </li>
                        <li>
                            <img src="../../dist/img/image-gallery-2.jpg" alt="14 APRIL 2016" data-description="Pengesahan Standarisasi Prosedur Operasional & Pelayanan (ISO)">
                        </li>
                        <li>
                            <img src="../../dist/img/image-gallery-3.jpg" alt="14 APRIL 2016" data-description="Rapat Evaluasi Pengadaan Barang/Jasa">
                        </li>
                        <li>
                            <img src="../../dist/img/banner.jpg" alt="14 APRIL 2016" data-description="Pengesahan Standarisasi Prosedur Operasional & Pelayanan (ISO)">
                        </li>
    				</ul>
    			</div>
			</div>
		</div>
	</div>

<footer class="footer hidden-sm hidden-xs">
    <div class="container">
        <div class="col-md-12">
            &copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
        </div>
    </div>
</footer>

<footer class="footer-xs hidden-lg hidden-md">
    <div class="container">
        <div class="col-md-12">
            &copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
        </div>
    </div>
</footer>

<?php include'../../layout/galeri_footer.php' ?>