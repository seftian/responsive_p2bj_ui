<?php include'../../layout/header.php' ?>   

<div class="content-body">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-12 col-xs-12">
				<div class="title-form grey-c" style="font-size: 2em;">Pengaduan</div>
				<div class="info-complaint">
					<div style="margin-bottom:30px;" class="grey">
						Sebelum melaporkan aduan, Anda bisa mencari aduan serupa melalui kotak pencarian dibawah.
                        <form action="">                                
							<p class="name input-search" style="margin-top: 50px;">
						        <input name="key" type="text" value="" class="validate[required,custom[onlyLetter],length[0,100]] feedback-input" placeholder="Cari aduan disini" id="input-search-p" />
						    </p>
                        </form>
				    </div>
				    <div>
				    	<!-- Repeat Pengaduan -->
				    	<div class="news-bottom-box-date light-grey writting-dintance">13 Mei 2016</div>
				    	<div class="news-complaint-text grey-c">
				    		Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
				    	</div>
				    	<div class="news-bottom-box-date grey"><i>Dian, Malang</i></div>
				    	<div class="news-complaint-answer">Jawaban :</div>
				    	<div class="news-complaint-text-answer grey">
			                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
				            <p class="hr"></p>
			            </div>
			            <!-- End repeat -->

			            <div class="news-bottom-box-date light-grey writting-dintance">13 Mei 2016</div>
				    	<div class="news-complaint-text grey-c">
				    		Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
				    	</div>
				    	<div class="news-bottom-box-date grey"><i>Dian, Malang</i></div>
				    	<div class="news-complaint-answer">Jawaban :</div>
				    	<div class="news-complaint-text-answer grey">
			                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
				            <p class="hr"></p>
			            </div>
                        
				    </div>
                    <p id="remove_btn">
			    	    <button type="button" id="btn_more" name="btn_more" class="button -greywhite">LOAD MORE</button>
				    </p>
				</div>
			</div>
			<div class="col-md-4 col-sm-12 col-xs-12 complaint-page">
				<div class="form-complaint-title form-complaint-title-page">
	            	Punya keluhan soal proses lelang? Laporkan pada kami disini!
	          	</div>
	          	<div class="form-complaint-input spacing" style="padding-top: 50px;">
	           		<form class="form" role="form" method="post" enctype="multipart/form-data" id="pengaduan">
						<p class="email">
							<input name="email" type="text" class="validate[required,custom[email]] feedback-input" id="email" placeholder="email" />
						</p>

						<p class="text">
							<textarea name="pengaduan" class="validate[required,length[6,300]] feedback-input" id="comment" placeholder="sampaikan keluhan disini..."></textarea>
						</p>

						<button type="button" class="button -red pull-right">KIRIM</button>
					</form>
	          	</div>
			</div>
		</div>
	</div>		
</div>

<footer class="footer hidden-sm hidden-xs">
    <div class="container">
        <div class="col-md-12">
            &copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
        </div>
    </div>
</footer>

<footer class="footer-xs hidden-lg hidden-md">
    <div class="container">
        <div class="col-md-12">
            &copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
        </div>
    </div>
</footer>

<?php include'../../layout/galeri_footer.php' ?>