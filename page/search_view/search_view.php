<?php include'../../layout/header.php' ?>

		<div class="content-body">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title-form grey-c" style="font-size: 2em;">Pencarian</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-7">
                    <form action="">
    					<p class="name input-search" style="margin-top: 50px;">
    				        <input name="key" type="text" value="" class="validate[required,custom[onlyLetter],length[0,100]] feedback-input" placeholder="Masukkan kata kunci pencarian" id="input-search-p" />
    				    </p>
                    </form>
				</div>
			</div>
			<p class="hr"></p> 
				<!-- Search yang ada gambarnya -->                     
                <div class="row">
			      <div class="col-xs-3">
				     <div class="search-image">
                        <img class="img-responsive" src="../../dist/img/image-gallery.jpg" />
                    </div>
			      </div>
			      <div class="col-xs-7 light-grey">
                    <a href="" class="search-view-title red">Kunjungan Kerja</a>
                    <p>
                        This is the most basic form of Bootstrap: precompiled files for quick drop-in usage in nearly any web project.                           
                    </p>
			      </div>
			      <div class="col-xs-2 text-center light-grey">
                    <p class="red">Pengumuman</p>
			      </div>
		       </div>
               <p class="hr"></p>
               <!-- End search -->

               <!-- Search yang non gambar -->
               <div class="row">
				    <div class="col-xs-10 light-grey">
	                    <a href="" class="search-view-title red">Kunjungan Kerja</a>
	                    <p>
	                        This is the most basic form of Bootstrap: precompiled files for quick drop-in usage in nearly any web project.                           
	                    </p>
				     </div>
				    <div class="col-xs-2 text-center light-grey">
	                    <p class="red">Pengumuman</p>
				    </div>
		       </div>
               <p class="hr"></p>
               <!-- End search -->
		</div>
	</div>


<footer class="footer hidden-sm hidden-xs">
	<div class="container">
		<div class="col-md-12">
			&copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
		</div>
	</div>
</footer>

<footer class="footer-xs hidden-lg hidden-md">
	<div class="container">
		<div class="col-md-12">
			&copy; 2016. PELAYANAN PENGADAAN BARANG / JASA, BADAN PENANAMAN MODAL PROVINSI JAWA TIMUR
		</div>
	</div>
</footer>

<?php include'../../layout/footer.php' ?>